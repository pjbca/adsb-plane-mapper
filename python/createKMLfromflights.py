
"""Upload the contents of your Downloads folder to Dropbox.

This is an example app for API v2.
"""

# processes csv of lat lon elev and outputs kml of all planes below 500 ' AGL near KSNA
# Peter Burke 6/14/2019
# use me like this:
# clear; python fileprocess.py 2019-06-01flights.txt 2019-06-01withHeliUnder500AGL.kml
# better yet:
# clear; python   /home/peter/Documents/adsbexchange/code/python/fileprocess.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-12-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-12-01withHeliUnder500AGL.kml' 

from __future__ import print_function


import argparse
import contextlib
import datetime
import os
import six
import sys
import time
import unicodedata
import zipfile
import datetime
import string
import filecmp
import json
from timeit import default_timer as timer
import re
import googlemaps
import cv2

# Google API: AIzaSyDyFnVe2cdxcQVGxDDGb-MJ4N4x-iYXEb0


if sys.version.startswith('2'):
    input = raw_input

parser = argparse.ArgumentParser(description='Convert file to file')
parser.add_argument('fileinputname', nargs='?', default='/home/peter/Documents/adsbexchange/inputtest.txt',
                    help='Input file name')

parser.add_argument('fileoutputname', nargs='?', default='/home/peter/Documents/adsbexchange/outputtest.txt',
                    help='Output file name')

parser.add_argument('HeliTrueORFalse', nargs='?', default='True',
                    help='Heli True OR False')

parser.add_argument('FWTrueORFalse', nargs='?', default='True',
                    help='Fixed Wing True OR False')

    
def main():
    """Main program.
    """
    args = parser.parse_args()
    fileinputname = os.path.expanduser(args.fileinputname)
    fileoutputname = os.path.expanduser(args.fileoutputname)
    HeliTrueORFalse = os.path.expanduser(args.HeliTrueORFalse)
    FWTrueORFalse = os.path.expanduser(args.FWTrueORFalse)

    DEMimagefilename = '/home/peter/Documents/adsbexchange/DEM/ASTGTM2_N33W118/ASTGTM2_N33W118_dem.tif'
    
    print('fileinputename=',fileinputname)
    print('fileoutputname=',fileoutputname)

    start_time = timer()

    include_heli_TorF=True
    #include_heli_TorF=False
    #include_non_heli_TorF=False
    include_non_heli_TorF=True

    
    if HeliTrueORFalse=='True': # process heli data
        include_heli_TorF=True
    elif HeliTrueORFalse=="False": # don't process heli data
        include_heli_TorF=False


    if FWTrueORFalse=='True': # process fixed wing data
        include_non_heli_TorF=True
    elif FWTrueORFalse=="False": # don't process fixed wing data
        include_non_heli_TorF=False
    





    print('include_heli_TorF = ',include_heli_TorF)
    print('include_heli_TorF = ',include_non_heli_TorF)
    
#    fileinputname='/home/peter/Documents/adsbexchange/inputtest.txt'
#    fileoutputname='/home/peter/Documents/adsbexchange/outputtest.txt'
        
    print('*************************************************************')
    print('loading DEM image')
    img = cv2.imread(DEMimagefilename,-1)
#    print(img)

        
# opencv 0,0 is top right, y to right x to down
    # from https://opencvvision.blogspot.com/

#    print(img[0,0]) # gives 273m El Monte? Google Earth gives 304 m
#    print(img[3600,0]) # gives 0 m Ocean? Google Earth gives 0
#    print(img[0,3600])  # gives 1034 m Big Bear? Google Earth gives 986 m
#    print(img[3600,3600])  # gives 357 m Poway? Google Earth gives 392 m

    # x=0 34 N LAT
    # x=3600 33 N LAT
    # y=0 -118 W LON
    # y = 3600 -117 W LON
    
    
 #   lattest=33.636074
 #   lontest=-117.845349
 #   pixelX=int(3600*(34-lattest))
 #   pixelY=int(3600* (118+lontest) )
 #   print(lattest,lontest,pixelX,pixelY,img[pixelX,pixelY])

    
 #   print(img.shape)
 #   print(img.size)
 #   print(img.dtype)
 #   print('*************************************************************')

    

    
    file_object_to_write_to = open(fileoutputname,'w')
    file_object_to_read_from = open(fileinputname,'r')


    prepend1 = '<Placemark>\n<styleUrl>#downArrowIcon</styleUrl>\n'
    prepend2 = '<Style>\n<IconStyle>\n<color>'
    prepend3 = 'ff364c00' #color in hex
    prepend4 = '</color>\n</IconStyle>\n'
    prepend5 = '<LabelStyle>\n<color>'
    prepend6 = 'ff364c00' #color in hex
    prepend7 = '</color>\n</LabelStyle>\n</Style>\n'
    prepend8='<Point><altitudeMode>absolute</altitudeMode><coordinates>'
    postpend = '</coordinates></Point>\n</Placemark>\n'  

    styletext1 = '<Style id="downArrowIcon">\n'
    styletext2 = '<IconStyle>\n<scale>0.3</scale>\n'

    styletext3 = '<Icon>\n'
    
    styletext4 ='<href>http://maps.google.com/mapfiles/kml/pal2/icon26.png</href>\n'
    
    #styletext4 ='<href>https://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href>\n'
    styletext5 ='</Icon>\n'
    styletext6 ='</IconStyle>\n'
    styletext7 ='</Style>\n'
    
    styletext=styletext1+styletext2+styletext3+styletext4+styletext5+styletext6+styletext7
    
    fileprepend = '<?xml version="1.0" encoding="UTF-8"?>\n<kml xmlns="http://earth.google.com/kml/2.0">\n<Document>\n'
    filepostpend = '</Document>\n</kml>'                                                                                         

 #   print('lat,lon,elev,alt,agl')
    
    file_object_to_write_to.write(fileprepend)
    file_object_to_write_to.write(styletext)
    
    line = file_object_to_read_from.readline()
    cnt = 0
    cnt_heli = 0
    while line:
        #print("Line {}: {}".format(cnt, line.strip()))
        sp=[]
        sp=(line.strip()).split(',')
        numlist=[]
        for st in sp:
            #print(st)
            #print( re.sub("[^\d\.-]", "", st) )
            numlist.append(re.sub("[^\d\.-]", "", st)) #numbers only, does not work for ICAO
        #print('***********************************')    
        for num in numlist:
            #print(num,type(num))
            if num == '' :
                x=0
                #print('skipping')
            else:
                newnum=float(num)
                #print(cnt,newnum)

        alt=float(numlist[2])
        lon=float(numlist[0])
        lat=float(numlist[1])
        pixelX=int(3600*(34-lat))
        pixelY=int(3600* (118+lon) )        
        elev=img[pixelX,pixelY]
        galt=float(numlist[3])
#        ICAO=numlist[4] # note this is text string need to parse separately
        ICAO=sp[4]
        #print(ICAO)
        type=int(numlist[5])

        descriptionstring = '<description>' + 'galt=' + str(galt) + ', ICAO = ' + ICAO + ', type = ' + str(type) + '</description>'

        
        if lat>34 or lat<33:
            elev=999999
        if lon<-118 or lon>-117:
            elev=999999


        # SW corner:
        latmin=33.628845
        lonmin=-117.957092
        # NE corner:
        latmax=33.717974
        lonmax=-117.788065
        altmax=152

        
#      results=googlemaps.Client('AIzaSyDyFnVe2cdxcQVGxDDGb-MJ4N4x-iYXEb0').elevation((lat,lon))
#        elev=results[0]['elevation']

        


#        print('***********************************')    
#        print('lat,lon, elev= ',lat,lon,elev)
        agl=alt-elev
#        agl=alt
#        print('alt elev agl =',alt,elev,agl)

#        print('lat,lon, elev= ',lat,lon,elev,'alt elev agl =',alt,elev,agl)
#        print('lat,lon, elev= ',lat,lon,elev,'alt elev agl =',alt,elev,agl)
#        print(lat,lon,elev,alt,agl)

 #       prepend3=colorfromalt(alt)
 #       prepend6=colorfromalt(alt)
        prepend3=colorfromalt(agl)
        prepend6=colorfromalt(agl)

#        prepend=prepend1+prepend2+prepend3+prepend4+prepend5+prepend6+prepend7+prepend8
        prepend=prepend1+descriptionstring+prepend2+prepend3+prepend4+prepend5+prepend6+prepend7+prepend8
        strtowrite=prepend+str(numlist[0])+','+str(numlist[1])+','+str(numlist[2])+'\n'+postpend
        if lat < latmax and lat > latmin and lon < lonmax and lon > lonmin and alt < altmax:
        #if agl<250 and : # only write to file if alt less than 500 ft AGL
            #file_object_to_write_to.write(strtowrite)
            cnt+=1
            if type==4: # check if heli here! make it true!
                cnt_heli+=1
                if include_heli_TorF == True:
                    file_object_to_write_to.write(strtowrite)
            else: # not heli! write to file!
                if include_non_heli_TorF == True:
                    file_object_to_write_to.write(strtowrite)
        line = file_object_to_read_from.readline()
#        cnt += 1

    

    file_object_to_write_to.write(filepostpend)

    file_object_to_write_to.close()
    file_object_to_read_from.close()
    
    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print('DONE DONE DONE')
    print('cnt = ',cnt)
    print('cnt_heli = ',cnt_heli)
    print('generated by ', __file__, ' on ',str(datetime.datetime.now()))



def colorfromalt(alt):
#    red=int(255*alt/2000)
#    green=int(255*alt/2000)
#    blue=int(255*alt/2000)
    r=int(255*red(alt,152)/65535)
    b=int(255*blue(alt,152)/65535)
    g=int(255*green(alt,152)/65535)

#    if alt<150: # make it red
#        r=int(255*red(0,610)/65535)
#        b=int(255*blue(0,610)/65535)
#        g=int(255*green(0,610)/65535)
        
    
    hx='%02x%02x%02x' % (b,g,r)

#    redhex=hex(r)[2:]
#    bluehex=hex(b)[2:]
#    greenhex=hex(g)[2:]    #    hexstring='ff364c00' #color in hex
#    hexstring='ff'+bluehex+greenhex+redhex
    hexstring='ff'+hx

#    print(alt,b,g,r,hx,hexstring)
#    print(alt,b,bluehex,g,greenhex,r,redhex,hx,hexstring)
    return hexstring




def red(f,n):
	# gives red part of rgb for rainbow from 0 to n, at point f.
	#variable/d f,n
	#variable/d fraction
	fraction = f/n
	if fraction<0.25 : # region one
		return 65535
	else:
		if fraction<0.5: # // region two
			return 65535 - (fraction-0.25)*(65535/0.25)
		else:

			if fraction<0.75: # // region three
				return 0
			else: # region four
				return 0

                            
def green(f,n):
	# gives green part of rgb for rainbow from 0 to n, at point f.
	#variable/d f,n
	#variable/d fraction
	fraction = f/n
	if fraction<0.25:# // region one
		return fraction*65535/0.25
	else:
		if fraction<0.5: #// region two
			return 65535
		else:

			if fraction<0.75: # // region three
				return 65535
			else: # // region four
				return 65535 - (fraction-0.75)*(65535/0.25)


def blue(f,n):
	#// gives blue part of rgb for rainbow from 0 to n, at point f.
	#variable/d f,n
	#variable/d fraction
	fraction = f/n
	if fraction<0.25:# // region one
		return 0
	else:
		if fraction<0.5: # // region two
			return 0
		else:

			if fraction<0.75:# // region three
				return (fraction-0.5)*(65535/0.25)
			else: # region four
				return 65535


if __name__ == '__main__':
    main()
