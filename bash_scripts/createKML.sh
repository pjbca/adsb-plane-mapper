#!/bin/bash

# Peter Burke 6/16/2019
# processes json files from adsbexchange

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}


echo "Starting..."

date
# Make sure you are in directory where createKMLfromflights.py is.
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-04-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-04-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-04-01onlyHeliUnder500AGL.kml' True False
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-05-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-05-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-05-01onlyHeliUnder500AGL.kml' True False
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-06-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-06-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018-06-01onlyHeliUnder500AGL.kml' True False
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-04-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-04-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-04-01onlyHeliUnder500AGL.kml' True False
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-05-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-05-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-05-01onlyHeliUnder500AGL.kml' True False
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-06-01withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-06-01noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019-06-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019-06-01onlyHeliUnder500AGL.kml' True False
# now do 2018 and 2019

cat '/home/peter/Documents/adsbexchange/Processed/Flights/2018-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/Flights/2018-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/Flights/2018-06-01flights.txt' > '/home/peter/Documents/adsbexchange/Processed/Flights/2018flights.txt'

cat '/home/peter/Documents/adsbexchange/Processed/Flights/2019-04-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/Flights/2019-05-01flights.txt' '/home/peter/Documents/adsbexchange/Processed/Flights/2019-06-01flights.txt' > '/home/peter/Documents/adsbexchange/Processed/Flights/2019flights.txt'

python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2018flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2018onlyHeliUnder500AGL.kml' True False

python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019withHeliUnder500AGL.kml' True True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019noHeliUnder500AGL.kml' False True
python createKMLfromflights.py '/home/peter/Documents/adsbexchange/Processed/Flights/2019flights.txt' '/home/peter/Documents/adsbexchange/Processed/KML/2019onlyHeliUnder500AGL.kml' True False


# done

date


echo "Done!"
echo "No further action should be required. Closing.."
