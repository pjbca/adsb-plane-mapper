#!/bin/bash

# Peter Burke 6/16/2019
# processes json files from adsbexchange

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}


echo "Starting..."

date

# do stuff here


#python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2018-04-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2018-04-01flights.txt' 

python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2018-05-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2018-05-01flights.txt'

python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2018-06-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2018-06-01flights.txt'

python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2019-04-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2019-04-01flights.txt'

python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2019-05-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2019-05-01flights.txt'

python /home/peter/Documents/adsbexchange/code/python/FilteradsbexchangeData.py '/home/peter/Documents/adsbexchange/data/unzipped/2019-06-01' '/home/peter/Documents/adsbexchange/Processed/Flights/2019-06-01flights.txt'



# done

date


echo "Done!"
echo "No further action should be required. Closing.."
