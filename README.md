# ADSB plane mapper

Takes ADSB data and makes a Google Earth compatible map of plane altitudes, converting MSL to AGL.

This is a set of code for a specific region (near John Wayne Airport KSNA).

You will have to modify it for your region of interest.

The lat/lon/max alt are hard coded for now.


# Raw data:

This repo is for parsing raw data from “adsbexchange”, which compiles aircraft location data from various sources, one of which is ADSB out data 
received by networks of thousands of volunteers around the globe which “feed” ADSB data from local receivers.

The format of the data is a JSON file of all aircraft around the world.
The fields are described here:
```
https://www.adsbexchange.com/datafields/
```

A file for a single day is approximately 20 GBytes. 


# Method:
## Step 1)
Use the python script:
``` FilteradsbexchangeData.py ```


Select only the data for planes near the aiport of interest.

It inputs json files from adsbexchange, filter them for planes near KSNA and below 2000', writes to a csv file.

The csv file has an entry for every aircraft near KSNA below 2000' MSL.
The entries are lat/lon, alt (MSL), time, type (e.g. fixed wing vs. heli), ICAO

To run it on a set of files, this bash script is provided as an example:
``` processJSONfiles.sh ```

## Step 2)
Use the python script:
``` createKMLfromflights.py ```

Reads the CSV file, and converts MSL to AGL using an elevation map.
The elevation map is a geotif file.
The name of the file is hard coded, and so is the conversion from x,y,z coordinate on the map to the lat/lon/elev.
``` ASTGTM2_N33W118_dem.tif ```
is the name of the file used.
The elevation data was taken from the digital elevation map (DEM) provided by the USGS
based on the Global Digital Elevation Model Version 2 created by the Advanced Spaceborne Thermal Mission And Reflection Radiometer (ASTER)
with 30 m resolution, downloaded from the USGS as a GeoTIFF.

These would have to be adjusted for other locations.

Aircraft locations below 500′ AGL are saved to a KML format file for inspection in Google Earth. 
The points are color coded from 0-500′ AGL (red zero, blue 500′). 
Aircraft on the ground are labeled as white points. 


The KML file can be read into Google Earth and inspected.

To run it on a set of files, this bash script is provided as an example:
``` createKML.sh ```

## Step 3) 
Use 
``` createFacMapKML.py ```

to create a square grid showing all the altitudes in the KSNA UAS facility map.
This shows the max altitude for drone flights per FAA.
See: 
```
https://www.faa.gov/uas/commercial_operators/uas_facility_maps/
```

The input is a text file with lat/lon/alt that has to be created from FAA data or manually.

The output is another kml file that can also be easily displayed in Google Earth.


# KML Files:

An output showing the SUAS FM created using step 3 above is provided for convenience:
``` KSNAfacilityMap.KML ```

Three KML files for getting the scale adjusted are also provided that can be used in Google Earth to set up the viewport:

``` ZoomInKSNA.kml ```

``` ZoomInKSNASoutheast.kml ```

``` ZoomOutKSNA.kml ```
